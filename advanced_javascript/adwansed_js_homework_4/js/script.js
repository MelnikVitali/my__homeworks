class TaskBoard {
    constructor() {
        this.addNewColumn = document.getElementById('add-new-column');

        this.addNewColumn.addEventListener('click', () => {
            this.buildNewColumn();
        });

        this.idCounter = 1;

        this.buildNewColumn();
    }

    buildNewColumn() {
        const newColumn = document.createElement('div');
        newColumn.classList.add('task-column');

        this.addNewColumn.before(newColumn);

        const taskColumnBlockTitle = document.createElement('div');
        taskColumnBlockTitle.classList.add('task-column__block-title');

        newColumn.append(taskColumnBlockTitle);

        const taskColumnTitle = document.createElement('input');
        taskColumnTitle.classList.add('task-column__title');
        taskColumnTitle.type = 'text';
        taskColumnTitle.value = 'Список дел';

        taskColumnBlockTitle.append(taskColumnTitle);

        const sortBtn = document.createElement('input');
        sortBtn.type = 'button';
        sortBtn.value = 'Сорт...';
        sortBtn.classList.add('task-column__sort-btn');

        taskColumnBlockTitle.append(sortBtn);

        sortBtn.addEventListener('click', this.sortCards.bind(this));

        const taskList = document.createElement('ul');
        taskList.classList.add('task-column__list');

        taskList.addEventListener('drop', this.drop.bind(this));
        taskList.addEventListener("dragover", this.allowDrop.bind(this));

        newColumn.append(taskList);

        const addNewTaskBtn = document.createElement('input');
        addNewTaskBtn.type = "button";
        addNewTaskBtn.value = "+ Добавить еще одну карточку";
        addNewTaskBtn.classList.add("task-column__add-btn");

        newColumn.append(addNewTaskBtn);

        addNewTaskBtn.addEventListener('click', () => {
            this.generateCard(taskList);
        });
    }

    generateCard(taskList, cardText = '') {
        const newTask = document.createElement('li');
        newTask.classList.add('task-card');
        newTask.dataset.id = `task-${this.idCounter}`;
        this.idCounter++;
        newTask.draggable = true;

        newTask.addEventListener("dragstart", this.drag.bind(this));

        taskList.append(newTask);

        const taskInput = document.createElement('textarea');
        taskInput.classList.add('task-card__message');
        taskInput.autofocus = true;
        taskInput.placeholder = 'Ввести заголовок для этой карточки';
        taskInput.innerText = cardText;
        taskInput.style.wordWrap = 'break-word ';

        newTask.append(taskInput);

        const addCardBtn = document.createElement('button');
        addCardBtn.classList.add('task-card__add-btn');
        addCardBtn.innerText = "Сохранить";

        addCardBtn.addEventListener('click', () => {
            taskInput.style.display = 'block';

            newTask.textContent = taskInput.value;

            newTask.append(closeBtn);
        });

        newTask.append(addCardBtn);

        const closeBtn = document.createElement('button');
        closeBtn.innerHTML = '&times;';
        closeBtn.className = "close-btn";
        closeBtn.title = "Удалить карточку";

        closeBtn.addEventListener('click', () => newTask.remove());

        newTask.append(closeBtn);

    }

    sortCards(event) {
        const currentColumn = event.target.parentElement.parentElement;

        const unsortedCards = currentColumn.querySelectorAll('.task-card');

        if (unsortedCards.length === 0) {
            return false;
        }

        const listCards = unsortedCards [0].parentElement;

        console.log(unsortedCards);

        const unsortedArray = [...unsortedCards];

        unsortedArray.forEach((item) => {
            listCards.removeChild(item);
        });

        const sortedCards = unsortedArray.sort((a, b) => {
            console.log(a.innerText);
            return a.innerText.toLowerCase() > b.innerText.toLowerCase() ? 1 : -1;
        });

        sortedCards.forEach(item => {
            listCards.append(item);
        });
    }

    drag(event) {
        event.dataTransfer.setData("text", event.target.dataset.id);
    }

    drop(event) {
        event.preventDefault();
        const data = event.dataTransfer.getData("text");

        if (event.target.classList.contains('task-card')) {
            event.target.before(document.querySelector(`[data-id='${data}']`));
        } else if (event.target.classList.contains('task-card__message')) {
            event.target.parentElement.after(document.querySelector(`[data-id='${data}']`));
        } else {
            event.target.appendChild(document.querySelector(`[data-id='${data}']`));
        }
    }

    allowDrop(event) {
        event.preventDefault();
    }
}

new TaskBoard();

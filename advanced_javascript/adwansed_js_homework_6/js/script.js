'use strict';
/*
## Задание

Получить список всех персонажей серии фильмов `Звездные войны` и вывести их на экран. По клику выводить под именем персонажа список космических кораблей, которыми он управлял (если конечно управлял).

#### Технические требования:
- Отправить AJAX запрос по адресу `https://swapi.co/api/people/` и получить список всех персонажей серии фильмов `Звездные войны`.
- Как только с сервера будет получена информация о персонажах, сразу же выведите их список на страницу. Необходимо указать имя, пол и родной мир персонажа (поля `name`, `gender` и `homeworld`).
- Если персонаж в фильмах управлял каким-то космическим кораблем или несколькими, вывести под названием его родного мира кнопку `Список кораблей` (свойство `starships`).
- При клике на кнопку `Список кораблей` получите с сервера все космические корабли, которыми персонаж управлял на протяжении саги, и выведите их на экран, заменив кнопку `Список кораблей` на тег h3 с текстом `Пилотируемые корабли` внутри него.
- AJAX запрос необходимо реализовать используя jQuery, с помощью `$.get()` или `$.ajax()`.
*/


$(document).ready(function () {
    for (let i = 1; i <= 9; i++) {
        $.get({
            url: `https://swapi.co/api/people/?page=${i}`,
            success: function (data) {
                const peoples = data.results;

                peoples.forEach(item => {

                    $.get({
                        url: item.homeworld,
                        success: function (response) {

                            const planet = response.name;
                            let button;
                            const div = document.createElement('div');
                            div.className = 'user';
                            div.id = item.name;
                            div.innerHTML = `<p><strong>Имя</strong>: ${item.name}</p><p><strong>Пол</strong>: ${item.gender}</p>
<p><strong>Родной мир</strong>: ${planet}</p>`;
                            $('.container').append(div);

                            if (item.starships.length > 0) {
                                button = document.createElement("button");
                                button.classList.add('btn-show-ships');
                                button.textContent = "Показать корабли";
                                div.append(button);
                            }

                            $(button).on('click', function () {
                                const starships = item.starships;
                                button.remove();
                                let strShips = '';
                                const p = document.createElement('p');
                                p.textContent = "Пилотируемые корабли :";
                                starships.forEach(link => {

                                    $.get({
                                        url: link,
                                        success: function (data) {
                                            strShips += ` ${data.name},`;
                                            p.innerHTML = `<strong>Пилотируемые корабли: </strong>${strShips.slice(0, -1)}`;
                                        },
                                        error: function (data) {
                                            console.log(data);
                                        }
                                    }, "json");
                                    div.append(p);
                                })
                            });
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    }, "json");
                })
            },
            error: function (data) {
                console.log(data);
            }
        }, "json");
    }
});





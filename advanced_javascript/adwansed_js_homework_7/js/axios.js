'use strict';
/*
Задание
Получить список всех планет из серии фильмов Звездные войны, и вывести на экран список персонажей, для которых эта планета - родной мир.

Технические требования:

Отправить AJAX запрос по адресу https://swapi.co/api/planets/ и получить список всех планет серии фильмов Звездные войны.
Для каждой планеты получить с сервера список персонажей, для которых она является родным миром. Список персонажей можно получить из свойства residents.
Как только с сервера будет получена информация о планетах, сразу же вывести список всех планет на экран. Необходимо указать имя планеты, климат, а также тип преобладающей местности (поля name, climate и terrain).
Как только с сервера будет получена информация о персонажах, родившихся на какой-то планете, вывести эту информацию на страницу под названием планеты.
Необходимо написать два варианта реализации в разных .js файлах. Один - с помощью fetch, другой - с помощью axios.
Чтобы все AJAX запросы получения персонажей выполнялись параллельно, в реализации fetch/axios список персонажей для каждой планеты необходимо получать с помощью функции Promise.all().
*/

window.onload = function () {
    const container = document.getElementById('container');
    axios.get('https://swapi.co/api/planets/')
        .then(response => {
            return response.data.results.map(item => {
                const div = document.createElement('div');
                div.className = 'film-card';
                div.id = item.name;
                div.setAttribute('data-target', `${item.residents}`);
                div.innerHTML += `<p class="film-item" id=""><strong>Имя: </strong> ${item.name}</p><p class="film-item"><strong>Климат: </strong>${item.climate}</p><p class="film-item"><strong>Местность: </strong>${item.terrain}</p>`;
                container.append(div);
                return div;
            })
        })
        .then(result => {
            result.forEach(element => {
                const id = element.id;
                let arr = [];
                let strPerson = '';
                if (element.getAttribute('data-target')) {
                    arr = element.getAttribute('data-target').split(',');
                }
                if (arr.length > 0) {
                    const p = document.createElement('p');
                    arr.forEach(item => {
                        Promise.all([
                            axios.get(item)
                                .then(person => {
                                    console.log(person.data);
                                    strPerson += ` ${person.data.name},`;
                                    p.innerHTML = `<strong>Персонажи: </strong>${strPerson.slice(0, -1)}`;
                                    document.getElementById(id).append(p);
                                })
                        ])
                    })
                }
            })
        })
};
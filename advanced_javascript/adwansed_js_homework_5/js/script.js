'use strict';
/*
Отправить AJAX запрос по адресу https://swapi.co/api/films/ и получить список всех фильмов серии Звездные войны.
Как только с сервера будет получена информация о фильмах, сразу же вывести список всех фильмов на страницу. Необходимо указать номер эпизода, название фильма, а также короткое содержание (поля episode_id, title и opening_crawl). Под этими данными вывести кнопку с надписью Список персонажей.
При клике на кнопку Список персонажей должны отправлятся AJAX-запросы на сервер для поисках всех персонажей саги, которые появлялись в этом фильме. API для получения каждого персонажа фильма можно получить из свойства characters ленты.
Как только с сервера будет получена информация о всех персонажах какого-либо фильма, вывести эту информацию на страницу под кнопкой Список персонажей для этого фильма, а саму кнопку можно удалить.
AJAX запрос нужно реализовать с помощью XMLHttpRequest.
*/

window.onload = function () {
    const requestFilms = new XMLHttpRequest();
    requestFilms.open('GET', 'https://swapi.co/api/films/');
    requestFilms.responseType = 'json';
    requestFilms.send();

    requestFilms.onload = function () {

        if (requestFilms.status !== 200) {
            console.log(`Error ${requestFilms.status}: ${requestFilms.statusText}`)
        } else {
            const container = document.getElementById('container'),
                resultRequest = requestFilms.response.results;

            resultRequest.forEach(item => {
                console.log(item.characters);
                const divCards = document.createElement('div');
                divCards.classList.add('card');
                divCards.innerHTML = `<p><strong>Title</strong>: ${item.title}</p><p><strong>Episode</strong>: ${item.episode_id}</p><p><strong>BackStory</strong>: ${item.opening_crawl}</p>`;

                const button = document.createElement("button");
                button.classList.add('btn-character-list');
                button.textContent = "Список Персонажей";
                divCards.append(button);

                button.addEventListener('click', function () {

                    const characters = item.characters;
                    button.remove();
                    let strPerson = '';
                    const divActors = document.createElement('div');
                    divActors.classList.add('actors-list');
                    console.log(characters);
                    characters.forEach(link => {
                        const requestCharacters = new XMLHttpRequest();
                        requestCharacters.open("GET", link);
                        requestCharacters.responseType = 'json';
                        requestCharacters.send();

                        requestCharacters.onload = function () {
                            if (requestCharacters.status !== 200) {
                                console.log(`${requestCharacters.status}, ${requestCharacters.statusText}`);
                            } else {
                                strPerson += ` ${requestCharacters.response.name},`;
                            }
                            divActors.innerHTML = `<strong>Actors</strong>: ${strPerson.slice(0, -1)}`;
                            divCards.append(divActors);
                        };
                    });
                });
                container.append(divCards);
            });
        }
    };
};
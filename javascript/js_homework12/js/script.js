'use strict';

const images = document.querySelectorAll(".image-to-show");
const title = document.getElementById('title');
const stopBtn = document.createElement('button');
stopBtn.innerText = 'Прекратить';
stopBtn.classList.add('btn-display');
const startBtn = document.createElement('button');
startBtn.innerText = 'Возобновить показ';
title.after(startBtn);
title.after(stopBtn);

let imgIndex = 0;
images[0].style.opacity = '1';
startBtn.disabled = true;

function showImages() {
    images.forEach((element, i) => {
        //прячем картинку за счет прозрачности
        element.style.opacity = '0';
        //получаем текущую картинку через остаток от деления счетчика на длину коллекции
        if (i === (imgIndex % images.length)){
            element.style.opacity = '1';
        } else {
            element.style.opacity = '0';
        }
    });
    imgIndex ++;
}

let timerId = setInterval(showImages, 10000);

stopBtn.addEventListener('click',()=> {
    clearInterval(timerId);
    //Отключение/включение кнопок "затенённый" цветовой эффект
    startBtn.disabled = false;
    stopBtn.disabled = true;
});
startBtn.addEventListener('click',() => {
    timerId = setInterval(showImages, 10000);
    startBtn.disabled = true;
    stopBtn.disabled = false;
});









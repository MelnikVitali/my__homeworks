'use strict';

function makeDeadLine(arrTeam, arrBackLog, deadData) {
    let teamPower = arrTeam.reduce((a, b) => a + b);
    let taskPower = arrBackLog.reduce((a, b) => a + b);
    let daysHave = Math.floor(((new Date(deadData)) - Date.now()) / (1000 * 60 * 60 * 24));//Convert milliseconds to days
    let workDays = daysWithoutHolidays(daysHave);
    let needDays = taskPower / teamPower;
    let refModule = Math.abs(needDays - workDays);//Math.abs - a negative number does a positive
    if (needDays < workDays) {
        return `Все задачи будут успешно выполнены за ${Math.round(refModule)} дней до наступления дедлайна!`
    } else if (needDays > workDays) {
        return `Команде разработчиков придется потратить дополнительно ${Math.round(refModule * 8)}  часов после дедлайна, чтобы выполнить все задачи в беклоге!`
    } else {
        return 'Good job , team   will make their work in time!'
    }
}

function daysWithoutHolidays(days) {
    let result = 0;
    let nowDate = new Date();
    for (let i = 0; i < days; i++) {
        nowDate.setDate(nowDate.getDate() + 1);
        if (nowDate.getDay() > 0 && nowDate.getDay() < 6) {
            result  += 1;
        }
    }
    return result ;
}
console.log(makeDeadLine([6, 4, 7, 8], [90, 70, 50, 82], new Date(2019, 9, 4)));
console.log(makeDeadLine([5, 6, 4, 7, 8, 4], [108, 87, 86, 82, 99, 645], new Date(2019, 9, 4)));


'use strict';

let arr = ['hello', 'world', 23, '23', null];

function filterBy(list, dataType) {
    return list.filter(function (item) {
        return typeof (item) !== dataType;
    });
}

console.log(filterBy(arr, 'string'));
console.log(filterBy(arr, 'number'));


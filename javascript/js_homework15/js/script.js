'use strict';
//********get and delete button to top************
const $btn = $('.to-top');
$(document).scroll(function() {
    //window height value
    const $screenHeight = $(window).innerHeight();
    //vertical scrolling window
    const $screenTop = $(window).scrollTop();

    if ($screenTop > $screenHeight) {
        $('.to-top').fadeIn('slow');
    } else {
        $('.to-top').fadeOut('slow');
    }
});
//scroll to zero when pressed
$btn.on('click',function() {
    $('html').animate({scrollTop: 0}, 500)
});

//**************animation for anchor links************
$('.header__menu-link').on('click', function (event) {
    event.preventDefault();
    const $href = $(this).attr('href');
    const $top = $($href).offset().top;
    $('body,html').animate({scrollTop: $top}, 400);
});

//*********** hide/show section 5(HOT NEWS)**********************
$('.section5__photo ').css({display:'none'});
$('.slide-toggle').on('click', function () {
    $('.section5__photo ').slideToggle('slow');
});



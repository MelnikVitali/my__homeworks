'use strict';

const changeThemeBtn = document.createElement('button');
changeThemeBtn.classList.add('change-btn', 'alt-theme');
document.body.prepend(changeThemeBtn);
changeThemeBtn.innerText = 'Сменить тему';
const wrapper = document.querySelector('.wrapper');

function changeTheme() {
    //перебираем коллекцию (.alt-theme) ставим метод toggle класслисту(есть класс удаляем/нет убираем)
    // document.querySelectorAll('.alt-theme').forEach((elem) => elem.classList.toggle('dark-theme'));
    //если на кнопке .dark-theme записываем в localStorage 'dark' иначе 'standard'
    wrapper.classList.toggle('dark-theme');

    if (wrapper.classList.contains('dark-theme')) {
        localStorage.setItem('userTheme', 'dark');
    } else {
        localStorage.setItem('userTheme', 'standard');
    }
}

//кликаем кнопку вызываем функцию changeTheme
changeThemeBtn.addEventListener('click', changeTheme);

//при перезагрузке страницы проверяем на черную тему в localStorage и вызваем функцию которая установит тему принудительно
if (localStorage.getItem('userTheme') === 'dark') {
    changeTheme()
}





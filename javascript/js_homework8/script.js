'use strict';
function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

let inputPrice = document.getElementById('input-price');

function getDataPrice(event) {
    const tempPrice = inputPrice.value;
    const errorStr = document.getElementById('error');
    const spanList = document.getElementById('span-list');

    if(tempPrice === ''){
        return;
    }
    else if (!isNumeric(tempPrice) || tempPrice < 0 ){
        errorStr.innerText = `Please enter correct price!!!`;
        inputPrice.classList.add('redBorder');
        event.currentTarget.style.color = ``;
    }else {
        inputPrice.classList.remove('redBorder');
        errorStr.innerText = ``;
        event.currentTarget.style.color = `green`;

        inputPrice.addEventListener('focus',()=> inputPrice.value = ``);

        const spanNode = document.createElement('span');
        spanNode.classList.add('span-item');

        const textNode = document.innerText = `Текущая цена: ${tempPrice}`;

        spanList.append(spanNode);
        spanNode.append(textNode);

        spanNode.addEventListener('click', () => {
            inputPrice.value = ``;
            spanNode.remove()
        });
    }
    return tempPrice;
}


inputPrice.addEventListener('blur', getDataPrice);


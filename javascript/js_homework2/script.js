'use strict';

//////////////////////Первое задание

//функция проверки на ввод, отсеивается всё, кроме строк-чисел и обычных чисел

function isNumeric(n) {
    return !isNaN(parseInt(n)) && isFinite(n);
}

function getNumbers() {
    let range = parseInt(prompt('Введите число', ''));
    while (!isNumeric(range)) {
        range = parseInt(prompt('Вы ввели не правильное значение!!! Введите число'));
    }
    return range;
}

function getMultiple(endNumbers) {

    let count = 0;//объявляем счетчик вызовов цыкла for
    for (let i = 1; i <= endNumbers; i++) {
        if (i % 5 === 0 && i !== 0) {
            count++;
            console.log(`multiple of 5 -> ${i}`);
        }
    }
    if (count === 0) {
        console.log('Sorry, no numbers');
    }
}

let userNumber = getNumbers();//записываем вызов функции и возвращенное ею значение в переменную

getMultiple(userNumber);


//////////////////////Второе задание


//функция проверки на ввод, отсеивается всё, кроме строк-чисел и обычных чисел, целые и больше одного

function validNumber(n) {
    return (!isNaN(parseFloat(n)) && isFinite(n) && Number.isInteger(n) && n > 1);
}


function getTwoNumbers() {
    let firstNumber = Number(prompt('Введите первое целое число больше 1', ''));
    while (!validNumber(firstNumber)) {
        firstNumber = Number(prompt('Вы ввели не правильное значение!!! Введите целое число больше 1'));
    }

    let secondNumber = Number(prompt('Введите второе целое число больше 1', ''));
    while (!validNumber(secondNumber)) {
        secondNumber = Number(prompt('Вы ввели не правильное значение!!! Введите целое число больше 1', ''));
    }

    // (меньшее из введенных чисел будет `start`, большее будет `end`)
    if (firstNumber > secondNumber) {
        let temp = firstNumber;

        firstNumber = secondNumber;
        secondNumber = temp;
    }
    //возвращаем массив
    return [firstNumber, secondNumber];
}


// Определяем функцию которая проверяет натуральное ли число или нет
function isNatural(range) {
    let start = range[0];
    let end = range[1];

    mark:
        for (let i = start; i <= end; i++) {// Для всех i oт стартового числа
            for (let j = 2; j < i; j++) {// проверить, делится ли число на какое-либо из чисел до него
                if (i % j === 0) continue mark;// не подходит, берём следующее
            }
            console.log(`Prime number -> ${i}`); // вывод в консоль простого числа
        }
}

let TwoUserNumbers = getTwoNumbers();//записываем вызов функции и возвращенное ею значение в переменную

isNatural(TwoUserNumbers);



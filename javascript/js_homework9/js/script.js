'use strict';

const tabs = document.querySelectorAll('.tabs-title');

tabs.forEach(function(item){
    item.addEventListener('click', function(){
        const currentSelector = ('.tabs-content__item[data-tabs-item="' + this.dataset.tabsTrigger + '"]');
        const currentTabData = document.querySelector(currentSelector);

        document.querySelector('.is-open').classList.remove('is-open');
        document.querySelector('.active').classList.remove('active');

        currentTabData.classList.add('is-open');
        this.classList.add('active');
    });
});
'use strict';


//input check function

function isNumeric(n) {
    return (!isNaN(parseFloat(n)) && isFinite(n) && Number.isInteger(n) && n >= 1);
}
//function to get a number from a user
function askNumber() {
    let number = parseInt(prompt('Введите целое число больше 0', ''));
    while (!isNumeric(number)) {
        number = parseInt(prompt('Вы ввели не правильное значение!!! Введите целое число больше 0'));
    }
    return number;
}


// **************Factorial through recursion*********
// function getFactorial(n) {
//     if (n === 1) {
//         return 1;
//     }
//     return n * getFactorial(n - 1);
// }
//************** Factorial through the cycle while****************
function getFactorial(n){
    let result = 1;
    while(n){
        result *= n--;
    }
    return result;
}
const number = askNumber();
alert(getFactorial(number));
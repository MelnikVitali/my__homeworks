'use strict';

//функция проверки на ввод, отсеивается всё, кроме строк-чисел и обычных чисел

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}


let userName = prompt('Enter your name', '');
while (parseInt(userName)
    || parseInt(userName) === 0
    || userName === null
    || userName === ''
    ) {
    alert("It's not a name!");
    userName = prompt('Enter your name', '');
}


let age = parseInt(prompt('Enter your age', ''));
while (!isNumeric(age)) {
    alert("It's not age!");
    age = parseInt(prompt('Enter your age', ''));
}

if (age < 18) {
    alert('You are not allowed to visit this website');
} else if (age >= 18 && age <= 22) {
    if (confirm('Are you sure you want to continue?')) {
        alert(`Welcome ${userName}`);
    } else {
        alert('You are not allowed to visit this website');
    }
} else {
    alert(`Welcome ${userName}`);
}